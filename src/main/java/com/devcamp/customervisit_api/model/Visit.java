package com.devcamp.customervisit_api.model;

import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }

    public String getName(){
        return this.customer.getName();
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public double getTotalExpense(){
        return this.serviceExpense + this.productExpense;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpense=" + productExpense
                + ", serviceExpense=" + serviceExpense + "]";
    }
    
}
